### ---------------------------------------------
''' With
with statement calls two methods behide the scense : __enter__ and __exit__ '''
class Log:
    def __init__(self,filename):
        self.filename=filename
        self.fp=None    
    def logging(self,text):
        self.fp.write(text+'\n')
    def __enter__(self):
        print("__enter__")
        self.fp=open(self.filename,"a+")
        return self    
    def __exit__(self, exc_type, exc_val, exc_tb):
        print("__exit__")
        self.fp.close()

with Log(r"C:\Users\SharpEl\Desktop\myfile.txt") as logfile:
    print("Main")
    logfile.logging("Test1")
    logfile.logging("Test2")


### ---------------------------------------------
''' Exceptions '''
try:
    f=open("test")
except (RuntimeError, NameError):
        print "execption"
except IOError as e:
    print "I/O error({0}): {1}".format(e.errno, e.strerror)
except ValueError:
    print "Could not convert data to an integer."
except: # or except Exception as e:
    print "Unexpected error:", sys.exc_info()[0]
else:
    f.close()
finally:
    print "finally"

''' Raising exception '''
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

try:
    raise MyError(2*2)
except MyError as e:
    print 'My exception occurred, value:', e.value

try:
    raise Exception('spam', 'eggs')
except Exception as inst:
    print type(inst)     # the exception instance
    print inst.args      # arguments stored in .args
    x, y = inst.args


### ---------------------------------------------
''' Function arguments � named, positional ''' 
def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
    pass

parrot(1000)                                # 1 positional argument
parrot(voltage=1000)                        # 1 keyword argument
parrot(voltage=1000000, action='VOOOOOM')        # 2 keyword arguments
parrot(action='VOOOOOM', voltage=1000000)        # 2 keyword arguments
parrot('a million', 'bereft of life', 'jump')       # 3 positional arguments
parrot('a thousand', state='pushing up the daisies')    # 1 positional, 1 keyword


### ---------------------------------------------
''' Function arguments � *args, **kwargs ''' 
def f(*args,**kwargs): print(args, kwargs)

f(1,2,3,"groovy")           # (1, 2, 3, 'groovy') {}
f(a=1,b=2,c=3)              # () {'a': 1, 'c': 3, 'b': 2}
f(1,2,3,a=1,b=2,c=3)        # (1, 2, 3) {'a': 1, 'c': 3, 'b': 2}

def f2(arg1,arg2,*args,**kwargs):
    print(arg1,arg2, args, kwargs)

f2(1,2,3,"groovy")              # 1 2 (3, 'groovy') {}
f2(arg1=1,arg2=2,c=3,zzz="hi")  # 1 2 () {'c': 3, 'zzz': 'hi'}
f2(1,2,3,a=1,b=2,c=3)           # 1 2 (3,) {'a': 1, 'c': 3, 'b': 2}
f2(1,1,q="winning")           # 1 1 () {'q': 'winning'}


### ---------------------------------------------
''' Unpacking arguments '''
range( *[3,7] ) # the same as range(3, 7) 
    
function_with_named_args( **{"name" :"ktos", "year": 1992})


### ---------------------------------------------
''' Decorators '''
def meet_dec(fn):
    def wrapper(name):
        return "{} Nice to meet you {}".format(fn(name),name)
    return wrapper
        
@meet_dec   
def hello(name):
    return "Hello {}".format(name)

def meet_dec_2(fn):
    def wrapper(*args, **kwargs):
        pass # just demostration 
    return wrapper

### ---------------------------------------------
''' Property decorator '''
class PropertyTest(object):
    def __init__(self, x):
        self._x = x
        
    @property
    def x(self):
        print "getter"
        return self._x
    
    @x.setter
    def x(self,value):
        print "setting x"
        self._x = value
        
    @x.deleter
    def x(self):
        print "deleting x - no no no"
        

### ---------------------------------------------
''' Decorator with argument '''
def tags(tag_name):
    def tags_decorator(func):
        def func_wrapper(name):
            return "<{0}>{1}</{0}>".format(tag_name, func(name))
        return func_wrapper
    return tags_decorator

@tags("p")
def get_text(name):
    return "Hello "+name


### ---------------------------------------------
''' Class method decorators '''
def p_decorate(func):
   def func_wrapper(self):
       return "<p>{0}</p>".format(func(self))
   return func_wrapper

class Person(object):
    def __init__(self):
        self.name = "John"
        self.family = "Doe"

    @p_decorate
    def get_fullname(self):
        return self.name+" "+self.family


### ---------------------------------------------
''' Decorators as class '''
class Counter(object):
    def __init__(self, f):
        self.f = f
        self.called = 0
    def __call__(self, *args, **kwargs):
        self.called += 1
        return self.f(*args, **kwargs)

@Counter
def bbb():
    pass

for i in range(3) : bbb()
print bbb.called


### ---------------------------------------------
''' Class inheritance '''
class C(A):
    def go(self):
        super(C, self).go()
        print("go C go!")


### ---------------------------------------------
''' Class - method __call__ '''
class Prz(object):
    ''' if method define, instance is callable '''
    def __call__(self, x, y ):
        print x+y
 
p=Prz()
p(1,2) # 3


### ---------------------------------------------
''' Locals '''
def test_locals(x):
    a=x+10
    print locals() # {'a': 11, 'x': 1}


### ---------------------------------------------
''' Dictionary fall back value '''
d={'hello': 'world'}
print d.get('hello', 'default_value') # world
print d.get('no_existing', 'default_value') # default_value


### ---------------------------------------------
''' List composition and/or lambda '''
a = [3, 4, 5]
b = [i for i in a if i > 4] # b = [4]
b = filter(lambda x: x>4, a)

a = [i+3 for i in a]
a = map(lambda x: x+3, a)


### ---------------------------------------------
''' Ignoring a value '''
fullfilename="test.txt"
filename,__,ext = fullfilename.rpartition('.') # ('test', '.', 'txt')
print file,ext # 'test', 'txt'


### ---------------------------------------------
''' Doc string'''
def test():
    ''' Text will be shown on help(test)'''
    pass


### ---------------------------------------------
''' Set arthmetics '''
s1=set((6,7))
s2=set((7,8))
s1 & s2 # set([7])
s1 | s2 # set([8, 6, 7])
s1 - s2 # set([6])


### ---------------------------------------------
''' Unit Test '''
import unittest
     
def func(x):
    return x + 2

class MyTest(unittest.TestCase):
    def test_1(self):
        self.assertEqual(func(3), 4)
        
    def test_2(self):
        self.assertTrue(func(3) == 4 )
        
    def test_3(self):
        self.assertGreater(func(3), 5, 'Message if bad')

python -m unittest module_under_test

### ---------------------------------------------
''' Built ins'''

# Sorting
x = [
 ['4', '21' ], 
 ['6', '16' ], 
 ['7', '5' ] ]
import operator
x.sort(key=operator.itemgetter(1))

# Ziping 
a = [1,2,3]; b = [4,5,6] ; c = [7,8]
x = [a]+[b]+[c] # [[1, 2, 3], [4, 5, 6], [7, 8]]
zip(*x)         # [(1, 4, 7), (2, 5, 8)]

# Eval input
a = [1, 2, 3]
input("enter command:")
>>> enter command:len(a) # 3

# Any / all
any([1>0, 1==0, 1<0]) # True
any([1<0, 2<1, 3<2]) # False

all(['a'<'b','b'<'c']) # True
all(['a'<'b','c'<'b']) # False

# Json
import json
a = {'4': 5, '6': 7}
b = json.dumps( a, sort_keys=True, indent=4, separators=(',', ': '))
c = json.loads( b )

### django

'''
Field lookups
'''
Entry.objects.(filter/get)
Entry.objects.filter(title__(i)regex=r'^(An?|The) +')
Entry.objects.filter(name__(i)exact='beatles blog')
Entry.objects.filter(headline__(i)contains='Lennon')
Entry.objects.filter(headline__(i)startswith='Will')
Entry.objects.filter(headline__(i)endswith='cats')

Entry.objects.filter(id=4) # exact is implicit
Entry.objects.filter(id__in=[1, 3, 4])
Entry.objects.filter(id__gt=4, id__lt=10) # gt, gte, lt, lte

Entry.objects.filter(pub_date__range=(start_date, end_date))

Question.objects.all()
Question.objects.order_by('-pub_date')[:5]
Question.objects.order_by('-pub_date')[2:10:2] # - in range is forbidden


from django.shortcuts import get_object_or_404

q = get_object_or_404(Question, pk=1)
q = get_object_or_404(Question, title__startswith='Q')
q = get_object_or_404(Question, title='Deep First Search')

from django.utils import timezone
current_year = timezone.now().year
Question.objects.get(pub_date__year=current_year)

# Django templates
# includes another template within the context
{% include "choices.html" %}
{% include "choices.html" with aditional_var=some_var %}

# Shows Lorem
{% lorem 2 %} # 2 paragrahs
{% lorem 150 w %} # 150 words

Pickling QuerySets
If you pickle a QuerySet, this will force all the results to be loaded into 
when you unpickle a QuerySet, it contains the results at the moment it 
was pickled, rather than the results that are currently in the database.

>>> Question.objects.get(id=2)


'''
Django QuerySet 
'''
count()
filter(**kwargs)
exclude(**kwargs)
annotate(*args, **kwargs) # Blog.objects.annotate(number_of_entries=Count('entry'))
order_by(*fields) # Entry.objects.order_by('blog__name', 'headline')
# Entry.objects.order_by(Coalesce('summary', 'headline').desc())
reverse()
distinct(*fields)
values(*fields)
all()
defer(*fields)
only(*fields)
delete()
create(**kwargs)

1)
p = Person.objects.create(first_name="Bruce", last_name="Springsteen")

2)
p = Person(first_name="Bruce", last_name="Springsteen")
p.save(force_insert=True)

get(**kwargs)
get_or_create(defaults=None, **kwargs)
# Returns a tuple of (object, created_flag), object- retrived or created, 
# and created_flag is a boolean specifying if object was created.

obj, created = Person.objects.get_or_create(
    first_name='John',
    last_name='Lennon',
    defaults={'birthday': date(1940, 10, 9)},
)


update(**kwargs)
update_or_create(defaults=None, **kwargs)

using(backup_db)

select_related() 
e = Entry.objects.get(id=5)
e = Entry.objects.select_related('blog').get(id=5)
e.blog (one precached, second second db hit)

prefetch_related(*lookups)

latest()
first()
last()

aggregate(*args, **kwargs)
Book.objects.all().aggregate(Max('price')) # price__max
Book.objects.all().aggregate(Avg('id')) # id__avg 

''' Aggregation functions '''
from django.db.models import Avg
Avg, Count, Max, Min, StdDev, Sum, exists()


##### 
''' Template tags '''
autoescape
block # access parent block - {{ block.super }}
comment
with
cycle
extends
filter
firstof
include
url
include
for
for ... empty
if  / ifequal / ifnotequal / ifchanged
lorem
now



csrf_token
debug
load

regroup
Grouping on other properties
spaceless
ssi
templatetag
verbatim
widthratio


''' Template filters '''
add
cut
date
default, default_if_none
dictsort / dictsortreversed
first / last
filesizeformat
floatformat
get_digit
join
length / length_is
random
lower / upper
title / capfirst
ljust / rjust / center
linenumbers
linebreaks / linebreaksbr 
pluralize
wordcount
wordwrap
yesno

make_list
unordered_list
addslashes




divisibleby

escape
escapejs
force_escape
iriencode




phone2numeric
pprint

safe
safeseq
slice
slugify
stringformat
striptags
time / timesince / timeuntil

truncatechars / truncatechars_html 
truncatewords / truncatewords_html

urlencode
urlize
urlizetrunc



















##############################################
'''
namedtuple

Tuples in python are immutable, but their value may change
when tuple holds a reference to an mutable object (ex. list)

'''

import collections
Card = collections.namedtuple('Card', ['rank', 'suit'])

c = Card() # error: __new__() takes exactly 3 arguments (1 given)
c = Card(rank='AS', suit='RedHeart')
print c # output: Card(rank='AS', suit='RedHeart')
print c.rank # output: 'AS'
c.rank = 123 # error: AttributeError: can't set attribute

c = Card(rank=[1, 2], suit='whatever')
c.rank=[1,2,3] # error: AttributeError: can't set attribute
c.rank.append(3)
print c.rank # output: [1, 2, 3]



'''
choice
'''

from random import choice
choice(range(10))



'''
new vs init


__new__ is called first, then __init__ will be called.

__new__ is a class method (but it get's special treatment, therefore decorator is omitted)

__new__ has the following arguments: class reference, then arguments which are passed to constructor.

__new__ must return an instance

__new__ instance returned will be 'self' in __init__

__init__ is forbidden from returning anything

__new__ returns
- instance of it�s own class, then __init__ will be called implicitly with self and arguments passed to __new__
- something else than instance of class, then __init__ will not be called. In this case you have to call __init__ method yourself.
'''

class Foo(object):
   def __new__(cls, *args, **kwargs):
      instance = super(Foo, cls).__new__(cls, *args, **kwargs)
      return instance

    def __init__(self, a, b):
        self.a = a
        self.b = b



'''
__dict__
'''

class Foo(object):
   def __init__(self, first, second):
      self.first = first
      self.__second = second


f = Foo(1, 2)
f.third = 3
f.__forth = 4
print f.__dict__

# output : {
#   'first': 1
#   '_Foo__second': 2,
#   'third': 3,
#   '__forth': 4,
# }



'''
new-style class
is any class which inherits from object.

New-style classes :
- are simply a user-defined types
- can use new features: __slots__, descriptors, properties, and __getattribute__
'''

class OldStyle:
   pass

class NewStyle(object):
   pass

old = OldStyle()
new = NewStyle()

type(old) # <type 'instance'>
type(new) # <class '__main__.NewStyle'>



'''
Collection

If a collection has no __contains__ method, the 'in' operator
does a sequential scan.
'''




'''
Special methods

ex __len__

Rather than invoking a special method, it's better to call the related built-in
function (eg. len, iter...). These builtin calls the corresponding special method,
but often provide other services (eg. for types like list, str, bytearray, and so on,
call returns the value of the ob_size field in the PyVarObject C struct - much faster
than calling a method).

Special method call can implicit - eg. the statement 'for i in x:' causes the invocation
of iter(x), which in turn may call x.__iter__() if that is available.

Some of special methods:
- add
- abs
- bool
- mul
- repr
'''



'''
__str__ vs __repr__

Rule of thumb:
- __repr__ is for developers (debugging and logging),
- __str__ is for customers (presentation to end users).

__repr__ should be unambiguous, if possible it should be source code necessary to recreate
the object being represented.

__str__  should return a string suitable for display to end users.

__repr__ will be used as fallback if __str__ in not available.
'''

class Foo(object):
   def __repr__(self):
      return 'call-repr'

   def __str__(self):
      return 'call-str'

foo = Foo()
print foo        # output: call-str
print str(foo)   # output: call-str
print repr(foo)  # output: call-repr
print '{0!s}   {0!r}'.format(foo) # output: call-str   call-repr




'''
__bool__

Python 3) Classes are true, unless __bool__ or __len__ is implemented. bool(x) calls x.__bool__();
if __bool__ is not implemented, Pythontries to invoke x.__len__(), and if that returns zero, bool
returns False. Otherwise bool returns True.

Python 2) uses __nonzero__() to evalute bool
'''



'''
Container vs Flat sequences

- container sequences: list, tuple, and collections.deque can hold items of different types.
- flat sequences: str, bytes, bytearray, memoryview, and array.array hold items of one type.

Container sequences hold references to the objects which may be of any type.
Flat sequences physically store the value of each item within its own memory.
Flat sequences are more compact, but are limited to holding primitive values
like characters, bytes, and numbers
'''



'''
list = []
dict = {}
tuple = ()
'''

d = (1, 2) # tuple
d = 1, 2   # tuple


'''
Container unpacking:
'''

foo = (1, 2)
print '{} / {}'.format(*foo) # output: 1 / 2
a, b = foo
print a # output: 1
print b # output: 2

foo = (13, 4)
divmod(*foo) # (3, 1)

# python3
a, b, *c = [1, 2, 3, 4, 5, 6]
print a # output: 1
print b # output: 2
print c # output [3, 4, 5, 6]


'''
In the context of parallel assignment, the * prefix can be applied
to exactly one variable, but it can appear in any position:
'''

a, *b, c = [1, 2, 3, 4, 5, 6] # will work as expected


'''
Powerful feature of tuple unpacking is that it works with nested structures.
'''


City = namedtuple('City', 'name country population coordinates')
tokyo = City('Tokyo', 'JP', 36.933, (35.689722, 139.691667))
print tokyo # output: City(name='Tokyo', country='JP', population=36.933, coordinates=(35.689722, 139.691667))



'''
Slicing

Slices and ranges excludes last item.
'''

s = 'bicycle'
s[::3]   # output: 'bye'
s[::-1]  # output: 'elcycib'
s[::-2]  # output: 'eccb'

l = list(range(10))
print l # output: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

[2:5] = [20, 30]
print l # output: [0, 1, 20, 30, 5, 6, 7, 8, 9]

del l[5:7]
print l # output: [0, 1, 20, 30, 5, 8, 9]

l[3::2] = [11, 22]
print l # output: [0, 1, 20, 11, 5, 22, 9]



'''
List * and +
'''

l = range(3)
print l # output: [0, 1, 2]

c = l * 3
print c # output: [0, 1, 2, 0, 1, 2, 0, 1, 2]

d = l + l
print d # output: [0, 1, 2, 0, 1, 2]



'''
Expressions a * SEQUENCE, where sequence contains mutable items,
the result may surprise you. Ex. trying to initialize a list of
lists as [[]] * 3 will result in a list with three references
to the same inner list
'''

a=[[0]]
print a # output: [[0]]

b = a * 3
print b # output: [[0], [0], [0]]

b[0][0] = 'watch-out'
print b # output: [['watch-out'], ['watch-out'], ['watch-out']]


board = [['_'] * 3 for i in range(3)]
print board # output: [['_', '_', '_'], ['_', '_', '_'], ['_', '_', '_']]

board[1][2] = 'GOOD'
print board # output: [['_', '_', '_'], ['_', '_', 'GOOD'], ['_', '_', '_']]

weird_board = [['_'] * 3] * 3
print weird_board # output: [['_', '_', '_'], ['_', '_', '_'], ['_', '_', '_']]

weird_board[1][2] = 'BAD'
print weird_board # output: [['_', '_', 'BAD'], ['_', '_', 'BAD'], ['_', '_', 'BAD']]



'''
*= and +=


+= uses __iadd__ (for �in-place addition�). If __iadd__ is not implemented, Python falls
 back to calling __add__.

For mutable sequences (list, bytearray, array.array), it will be changed in place.
When __iadd__ is not implemented, the expression a += b has the same effect as a = a + b:
'''

l = [1, 2, 3]
print id(l) # output: 4311953800

l *= 2
print l     # output: [1, 2, 3, 1, 2, 3]
print id(l) # output: 4311953800

t = (1, 2, 3)
print  id(t) # output: 4312681568

t *= 2
print t     # output: (1, 2, 3, 1, 2, 3)
print id(t) # output: 4301348296
                                                   
                                                   
##############
##############
############## 


