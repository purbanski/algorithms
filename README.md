# Algorithms #

My solutions to different algorithms problems found on Codility, HakerRank, LeetCode etc. 
Mostly written in Python, some of them are coded in C++