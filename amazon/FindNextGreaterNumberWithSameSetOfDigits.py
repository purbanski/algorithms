#!/usr/bin/python

'''
source: http://www.geeksforgeeks.org/find-next-greater-number-set-digits/

Find next greater number with same set of digits
Given a number n, find the smallest number that has same set of digits as n and is greater than n. 
If x is the greatest possible number with its set of digits, then print 'not possible'.

Examples:
For simplicity of implementation, we have considered input number as a string.

Input:  n = "218765"
Output: "251678"

Input:  n = "1234"
Output: "1243"

Input: n = "4321"
Output: "Not Possible"

Input: n = "534976"
Output: "536479"
'''

def solution(n):
    list_to_number = lambda l : int(''.join([str(i) for i in l]))
    
    not_possible = "Not Possible"
    a = [int(i) for i in str(n)]
    
    if len(a) == 1:
        print not_possible
        return list_to_number(a)
        
    ind = None
    for i in reversed(range(len(a)-1)):
        if a[i]<a[i+1]:
            ind = i
            break

    if ind is None:
        print not_possible
        return list_to_number(a)
    
    a[ind+1:]=reversed(a[ind+1:])

    for i in range(ind+1, len(a)):
        if a[i]>a[ind]:
            a[i],a[ind] = a[ind],a[i]
            break
    return int(''.join([str(i) for i in a]))


def check(n,m):
    a = solution(n)

    if a!=m: 
        print "Problem with {}".format(n)
        print "\t exp:{}".format(m)
        print "\t got:{}".format(a) 
    else:
        print "Ok with {} got {}".format(n,a)
        
    
check(218765,251678)
check(1234, 1243)
check(4321, 4321)
check(534976, 536479)
check(534976532, 535234679)
