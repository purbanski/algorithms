#!/usr/bin/python

'''
source: https://www.quora.com/What-are-the-questions-asked-in-an-Amazon-Online-Test-in-HackerRank#

3. Maximize

A swap operation M on an array is defined where you can only swap the
adjacent elements. Given an array containing digits and n swap
operations(defined as below), maximize the value of the array.
Example: Array 1, 2, 4, 3 (value = 1243), Number of swaps 2

Output 4,1,2,3 (value = 4123).

'''

def solution(n, swap):
    a = [int(i) for i in n.split(',')]
    
    run = 0
    while (swap>0 and run< len(a)):
        max_value = max(a[run:run+swap+1])
        max_index = (a[run:run+swap+1]).index(max_value)+run
        
        if max_value != a[run]:
            move = max_index - run
            
            if move <= swap:
                swap -= move
                a.pop(max_index)
                a.insert(run, max_value)
            else:
                a.pop(max_index)
                a.insert(max_index-swap, max_value)
                swap = 0
                
        run += 1
    print a

    

solution("3, 8, 9",1) 
# solution("1,2,3,4",3)
# solution("1,2,3,4",4)
# solution("1,2,3,4",5)
# solution("1,2,3,4",6)
# solution("1,2,3,4",7)
# solution("1,2,3,4",8)
# solution("1,2,3,4",9)



# solution(raw_input())