#!/usr/bin/python

'''
source: https://www.quora.com/What-are-the-questions-asked-in-an-Amazon-Online-Test-in-HackerRank#

2. Most Frequent
Given an integer array, find the most frequent number and it's count in the array. Write the
code in O(1) space. Eg 1 , 3, 4, 5, 2, 2, 3, 2 Output Most frequent number is 2. The
frequency is 3. Return the output as string in 'number: frequency' format. e.g. 2: 3 (Please
note the space after : and frequency. If multiple numbers have the same highest frequency
return the smallest number.
'''

def solution(a):
    
    if len(a) == 1:
        return a[0]
    
    s = sorted(a)
    prev_value = s[0]
    next_value = s[1]
    
    index = 0
    while index < len(s)-2:
        if prev_value != next_value:
            print prev_value
            break
        
        index += 1
        prev_value = next_value
        next_value = s[index]
    
    if s[-1] != s [-2]:
        print s[-1]
    

solution("dupdupa")
solution("adupdup")
solution("dfadf")
solution("da")
solution("d")