#!/usr/bin/python

'''
source: https://www.quora.com/What-are-the-questions-asked-in-an-Amazon-Online-Test-in-HackerRank#

2. Most Frequent

Given an integer array, find the most frequent number and it's count in the array. Write the
code in O(1) space. Eg 1 , 3, 4, 5, 2, 2, 3, 2 Output Most frequent number is 2. The
frequency is 3. Return the output as string in 'number: frequency' format. e.g. 2: 3 (Please
note the space after : and frequency. If multiple numbers have the same highest frequency
return the smallest number.
'''

'''
Solution using sort
'''
def solution(n):
    a = [int(i) for i in n.split(',')]
    a.sort()
    
    fre = fre_temp = 1
    no = no_temp = a[0]
    
    for i in a[1:]:
        if no_temp == i:
            fre_temp += 1
        else:
            no_temp = i
            fre_temp = 1
        
        if fre_temp>fre:
            fre = fre_temp
            no = no_temp
        elif fre_temp==fre and no_temp < no:
            no = no_temp
            
    print "{}: {} ".format(no,fre)


'''
Solution using hash maps
'''
    
def solution2(n): 
    a = [int(i) for i in n.split(',')]
    m = {}
    
    for i in a:
        if i in m:
            m[i] = m[i]+1
        else:
            m[i] = 1
            
    freq = sorted(m.values())[-1]
    no   = m.keys().index(freq)

    print "{}: {} ".format(no,freq)
    
        
solution2('1 , 3, 4, 5, 2, 2, 3, 2')
# solution(raw_input())