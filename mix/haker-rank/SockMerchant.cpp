//
//  SockMerchant.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 10/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include "haker-rank.hpp"

namespace SockMerchant {
    int solution(vector<int> socks) {
        if (socks.size() < 2 )
            return 0 ;
        
        std::sort(socks.begin(), socks.end());
        
        int socksPairCount = 0;
        int id = 0;
        
        while( id < socks.size() - 1 ) {
            if (socks[id] == socks[id+1]) {
                socksPairCount++;
                id += 2;
            } else {
                id++;
            }
        }
        
        return socksPairCount;
    }

}
