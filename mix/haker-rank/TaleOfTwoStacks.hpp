//
//  TaleOfTwoStacks.h
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 09/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef TaleOfTwoStacks_h
#define TaleOfTwoStacks_h

#include <stack>

using namespace std;

namespace TaleOfTwoStacks {
    
    class MyQueue {
        
    public:
        using IntStack = stack<int>;
        IntStack stack_newest_on_top, stack_oldest_on_top;
        
        void push(int x) {
            stack_newest_on_top.push(x);
        }
        
        void pop() {
            if (( ! stack_oldest_on_top.size() ) &&
                ( ! stack_newest_on_top.size() )) {
                // nothing to pop
                return;
            }
            
            if ( ! stack_oldest_on_top.size() ) {
                updateOldestOnTopStack();
            }
            
            stack_oldest_on_top.pop();
        }
        
        int front() {
            if (( ! stack_oldest_on_top.size() ) &&
                ( ! stack_newest_on_top.size() )) {
                throw "queue is empty";
            }
            
            if ( ! stack_oldest_on_top.size() ) {
                updateOldestOnTopStack();
            }
            
            return stack_oldest_on_top.top();
        }
        
        int size() const {
            return stack_newest_on_top.size() + stack_oldest_on_top.size();
        }
    private:
        void updateOldestOnTopStack() {
            moveToStack(stack_newest_on_top, stack_oldest_on_top);
        }
        
        void moveToStack( IntStack& src , IntStack& dst ) {
            while (src.size()) {
                dst.push( src.top() );
                src.pop();
            }
            
        }
    };
    
}


#endif /* TaleOfTwoStacks_h */
