//
//  Contacts.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 10/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef Contacts_hpp
#define Contacts_hpp

#include <stdio.h>
#include <map>
#include <string>
#include <iostream>
#include "haker-rank.hpp"

using namespace std;

//------

class TryNode {
public:
    TryNode() {
        _completeWord = false;
        _childWordsCount = 0;
    }

    TryNode(bool completeWord) {
        _completeWord = completeWord;
        _childWordsCount = 0;
    }

    void increaseChildWordsCount() {
        _childWordsCount++;
    }
    
    // count words starting with name
    int count(const string& name) {
        if (name.length() == 0) {
            return 0;
        }

        char key = name.at(0);
        auto element = _children.find(key);
        
        if (element == _children.end()) {
            return 0;
        } else {
            TryNode* nextNode = element->second;
            string chopedName = name.substr(1, name.length());
            bool moreCharsToProcess = (chopedName.length() == 0 ? false : true );
            
            if ( moreCharsToProcess ) {
                return nextNode->count(chopedName);
            } else {
                return nextNode->countCompleteWords();
            }
        }
    }
    
    int countCompleteWords() {
        return _childWordsCount;
        
    }
    
    void add(const string& name) {
        if (name.length() == 0) {
            return;
        }
        add_(name, false);
    }
    
private:
    bool add_(const string& name, bool newWordAdded) {
        
        char key = name.at(0);
        string chopedName = name.substr(1, name.length());
        bool moreCharsToProcess = (chopedName.length() == 0 ? false : true );
        
        auto element = _children.find(key);
        if (element == _children.end()) {
            // not found
            
            TryNode *newNode = new TryNode(! moreCharsToProcess);
            _children[key] = newNode;
            newWordAdded = true;
            
            if ( moreCharsToProcess ) {
                newWordAdded = newNode->add_(chopedName, true);
            } else {
                newNode->increaseChildWordsCount();
            }
        }
        else {
            if (moreCharsToProcess) {
                newWordAdded = element->second->add_(chopedName, false);
            }
        }
        
        if (newWordAdded) {
            _childWordsCount++;
        }
        
        return newWordAdded;
    }
    
    
private:
    map<char, TryNode*> _children;
    
    int  _childWordsCount;
    bool _completeWord;
};

//---

class TriesTree {
public:
    TriesTree() {
        
    }
    
    ~TriesTree() {
        // iterate and delete memory
    }

    void add(const string& name) {
        _root.add(name);
    }
    
    int count(const string& name) {
        return _root.count(name);
    }
    
private:
    TryNode _root;
};


// As a class not namespace - so it fits into header
class Contacts {

public:
    static void solution(const string& operation, const string& name) {
        
        if (operation == "add") {
            operationAdd(name);
        }
        else if (operation == "find") {
            operationFind(name);
        }
        else {
            // invalid operation
        }
    };
    
private:
    static void operationAdd(const string& name) {
        _contacts.add(name);
    }

    static void operationFind(const string& name) {
        cout << _contacts.count(name) << endl;
    }

private:
    static TriesTree _contacts;
};

TriesTree Contacts::_contacts;


#endif /* Contacts_hpp */
