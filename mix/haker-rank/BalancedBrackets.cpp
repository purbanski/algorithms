//
//  BalancedBrackets.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 09/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <string>
#include <stack>

using namespace std;

namespace BalancedBrackets {
    
    bool is_balanced(string expression) {
        stack<char> brackets;
        
        for ( auto c : expression ) {
            if ( c == '{'  || c == '[' || c == '(' ) {
                brackets.push(c);
            } else if ( c == '}'  || c == ']' || c == ')' ) {
                
                if ( brackets.size() == 0 ) return false;
                
                char b = brackets.top();
                brackets.pop();
                
                if ( b == '{' && c != '}' ) return false;
                if ( b == '[' && c != ']' ) return false;
                if ( b == '(' && c != ')' ) return false;
                
            } else {
                // other characters - ignore them
            }
        }
        
        return brackets.size() == 0;
    }
    
}
