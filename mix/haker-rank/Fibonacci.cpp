//
//  Fibonacci.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 10/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>

namespace Fibonnaci {

    int fibNumbers[41];
    
    int fibonacci(int n) {
        fibNumbers[0] = 0;
        fibNumbers[1] = 1;
    
        for (int i=2; i<=n; i++) {
            fibNumbers[i] = fibNumbers[i-2]+fibNumbers[i-1];
        }
        return fibNumbers[n];
    }
    
    int fibonacci2(int n) {
        int back2 = 0;
        int back1 = 1;
        int ret = 0;
        
        for (int i=2; i<=n; i++) {
            ret = back2 + back1;
            back2 = back1;
            back1 = ret;
        }
        return ret;
    }
}
