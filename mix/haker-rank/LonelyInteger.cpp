//
//  LonelyInteger.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 10/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include "haker-rank.hpp"

namespace LonelyInteger {
    int lonely_integer(vector < int > a) {
        int ret = 0;
        for (auto e : a ) {
            ret ^= e;
        }
        return ret;
    }
}
