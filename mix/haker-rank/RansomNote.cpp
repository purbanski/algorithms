//
//  RansomNote.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 09/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <string>
#include <map>
#include <vector>

#include "haker-rank.hpp"

namespace RansomNote {
    bool ransom_note(vector<string> magazine,
                     vector<string> ransom) {
        
        map<string, int> wordsAvailable;
  
        for (const string& word : magazine) {
            
            if (wordsAvailable.find(word) == wordsAvailable.end() ) {
                wordsAvailable[word] = 1;
            } else {
                wordsAvailable[word]++;
            }
        }

        for (const string& word : ransom) {
        
            if (wordsAvailable.find(word) == wordsAvailable.end()) {
                return false;
            } else {
                int wordCount = wordsAvailable.at(word);
                if ( wordCount == 0 ) {
                    return false;
                } else {
                    wordsAvailable[word]--;
                }
            }
        }
        
        return true;
    }
    

}
