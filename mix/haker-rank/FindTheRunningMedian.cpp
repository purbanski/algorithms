//
//  FindTheRunningMedian.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 10/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "FindTheRunningMedian.hpp"

namespace FindTheRunningMedian {
    void printMedian(float m) {
        printf("%.1f\n", m);
    }
    
    void printMedian(int l, int r) {
        printMedian(static_cast<float>(l+r) / 2.0f);
    }
    
    void solution(vector<int> input) {
       
        printMedian(input[0]);
        if ( input.size() == 1 ) {
            return;
        }
        
        printMedian(input[0], input[1]);
        if ( input.size() == 2 ) {
            return;
        }
        
        MaxHeap<int> maxHeap;
        MinHeap<int> minHeap;
        
        int biggerValue = max(input[0], input[1]);
        int smallerValue = min(input[0], input[1]);
        
        maxHeap.add(smallerValue);
        minHeap.add(biggerValue);
        
        for( int i=2; i<input.size(); i++) {
            int maxHeapTop = maxHeap.top();
            int element = input[i];
            
            if (maxHeapTop >= element) {
                maxHeap.add(element);
            } else {
                minHeap.add(element);
            }
            
            // rearrange heaps so their sizes are not
            // are only bigger/smaller by 1 at the most
            if ( abs( maxHeap.size() - minHeap.size() ) > 1 ) {
                if ( maxHeap.size() > minHeap.size() ) {
                    // move top from max to min
                    int top = maxHeap.popTop();
                    minHeap.add(top);
                } else {
                    // move top from min to max
                    int top = minHeap.popTop();
                    maxHeap.add(top);
                }
            }
            
            if (maxHeap.size() == minHeap.size()) {
                printMedian(maxHeap.top(), minHeap.top());
            }
            else if (maxHeap.size() > minHeap.size()) {
                printMedian(maxHeap.top());
            }
            else {
                printMedian(minHeap.top());
            }
        }
    }
}
