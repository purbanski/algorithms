//
//  MakingAnagrams.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 08/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "haker-rank.hpp"
#include <string>


namespace MakingAnagrams {
    int number_needed(string a, string b) {
        using CharCountMap = map<char, int>;
        
        CharCountMap mapA;
        CharCountMap mapB;
        
        for (auto c : a) {
            if ( mapA.find(c) == mapA.end() ) {
                mapA[c] = 1;
            } else {
                mapA[c] = mapA[c]+1;
            }
        }
        
        int ret = 0;
        for (auto c : b) {
            if ( mapA.find(c) == mapA.end() ) {
                ret++;
            } else {
                if (mapA.at(c) == 0 ) {
                    ret++;
                } else {
                    mapA.at(c)--;
                }
            }
        }
        
        for (pair<char,int> e : mapA) {
            ret += e.second;
        }
        return ret;
    }
}


