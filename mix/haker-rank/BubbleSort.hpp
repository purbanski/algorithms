//
//  BubbleSort.h
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 11/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef BubbleSort_hpp
#define BubbleSort_hpp

#include <vector>
#include <iostream>

using namespace std;

namespace BubbleSort {
vector<int> solution(vector<int> input) {
    
    int n = input.size();
    int numberOfSwaps = 0;

    for (int i = 0; i < n; i++) {
        // Track number of elements swapped during a single array traversal
//        int numberOfSwaps = 0;
        
        for (int j = 0; j < n - 1; j++) {
            // Swap adjacent elements if they are in decreasing order
            if (input[j] > input[j + 1]) {
                swap(input[j], input[j + 1]);
                numberOfSwaps++;
            }
        }
        
        // If no elements were swapped during a traversal, array is sorted
        if (numberOfSwaps == 0) {
            break;
        }
    }
    
    cout << "Array is sorted in " << numberOfSwaps << " swaps." << endl;
    cout << "First Element: " << input[0] << endl;
    cout << "Last Element: " << input[n-1] << endl;
    
    return input;
}
    
}

#endif /* BubbleSort_hpp */
