//
//  FindTheRunningMedian.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 10/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef FindTheRunningMedian_hpp
#define FindTheRunningMedian_hpp

#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

//-------------

template <class T>
class Heap {

public:
    vector<T> _container;
    
protected:
    Heap() {
    }
    
    Heap (initializer_list<T> list ) {
        for ( const T& element : list ) {
            _container.push_back(element);
        }
    }

    
    virtual void makeHeap() = 0;

public:
    virtual void add(T element) =  0;

    T top() const {
        // check size of container
        return Heap<T>::_container.at(0);
    }
    
    virtual T popTop() = 0;
    
    void dump() const {
        cerr << "heap: ";
        for ( const T& element : _container ) {
            cerr << element << ", ";
        }
        cerr << endl;
    }
    
    int size() const {
        return static_cast<int>( _container.size() );
    }

};

//--------

template <class T>
class MaxHeap : public Heap<T> {
public:
    MaxHeap() : Heap<T>() {}
    MaxHeap(initializer_list<T> list) : Heap<T>(list) {
        makeHeap();
    }
    
    virtual void add(T element) {
        Heap<T>::_container.push_back(element);
        push_heap(Heap<T>::_container.begin(), Heap<T>::_container.end());
    }

    virtual T popTop() {
        // check size
        T ret = Heap<T>::top();

        pop_heap(Heap<T>::_container.begin(), Heap<T>::_container.end());
        Heap<T>::_container.pop_back();

        return ret;
    };

protected:
    virtual void makeHeap() {
        make_heap(Heap<T>::_container.begin(), Heap<T>::_container.end());
    }
    
};

//---------


template <class T>
class MinHeap : public Heap<T> {
public:
    MinHeap() : Heap<T>() {}
    MinHeap(initializer_list<T> list) : Heap<T>(list) {
        makeHeap();
    }
    
    virtual void add(T element) {
        Heap<T>::_container.push_back(element);
        push_heap(Heap<T>::_container.begin(), Heap<T>::_container.end(), cmp);
    }

    virtual T popTop() {
        // check size
        T ret = Heap<T>::top();
        
        pop_heap(Heap<T>::_container.begin(), Heap<T>::_container.end(), cmp);
        Heap<T>::_container.pop_back();
        
        return ret;
    };
    
protected:
    virtual void makeHeap() {
        make_heap(Heap<T>::_container.begin(), Heap<T>::_container.end(), cmp);
    }

private:
    static bool cmp(T a, T b)  {
        return a > b;
    }
};

//namespace FindTheRunningMedian {
namespace FindTheRunningMedian {
    void solution(vector<int> input);
}


#endif /* FindTheRunningMedian_hpp */
