//
//  MakingAnagrams.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 08/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef MakingAnagrams_hpp
#define MakingAnagrams_hpp

#include "TaleOfTwoStacks.hpp"
#include <stdio.h>

#include <map>
#include <vector>
#include <stack>

using namespace std;

namespace MakingAnagrams {
    int number_needed(string a, string b);
}

namespace BalancedBrackets {
    bool is_balanced(string expression);
}

namespace RansomNote {
    bool ransom_note(vector<string> magazine,
                     vector<string> ransom);
}

namespace SockMerchant {
    int solution(vector<int> socks);
}

namespace LonelyInteger {
    int lonely_integer(vector < int > a);
}

namespace Fibonnaci {
    int fibonacci(int n);
    int fibonacci2(int n);
}

#endif /* MakingAnagrams_hpp */
