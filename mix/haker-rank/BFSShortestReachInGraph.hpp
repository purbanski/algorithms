//
//  BFSShortestReachInGraph.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 11/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef BFSShortestReachInGraph_hpp
#define BFSShortestReachInGraph_hpp

#include <stdio.h>
#include <iostream>
#include <forward_list>
#include <vector>
#include <queue>
#include <list>
#include <set>

using namespace std;

namespace BFSShortestReachInGraph {
    

class Node {
    list<int> _adjacent;

public:
    Node() {
    }
    
    void addNode(int nodeId) {
        _adjacent.push_front(nodeId);
    }
    
    const list<int>& getNodes() {
        return _adjacent;
    }
};

class Graph {
    vector<Node*> _nodes;
    
public:
    Graph(int n) {
        _nodes = vector<Node*>(n);
        for (int i=0; i<n; i++) {
            _nodes[i] = new Node();
        }
    }
    
    ~Graph() {
        for (auto node : _nodes) {
            delete node;
        }
    }
    
    void add_edge(int u, int v) {
        Node* nodeU = _nodes[u];
        Node* nodeV = _nodes[v];
        
        nodeU->addNode(v);
        nodeV->addNode(u);
    }
    
    vector<int> shortest_reach(int startId) {
        const int distanceWeight = 6;
        
        vector<int> distances(_nodes.size(), -1);
        distances[startId] = 0;
        
        set<int> visited;
        visited.insert(startId);
        
        queue<int> toVisit;
        toVisit.push(startId);
        
        while (toVisit.size()) {

            int currentNodeId = toVisit.front();
            toVisit.pop();

            list<int> adjacentNodes = _nodes[currentNodeId]->getNodes();
            for (int adjacentNodeId : adjacentNodes ) {

                if (visited.find(adjacentNodeId) != visited.end()) {
                    continue;
                }
                
                toVisit.push(adjacentNodeId);
                distances[adjacentNodeId] = distances[currentNodeId] + distanceWeight;
                visited.insert(adjacentNodeId);
            }
        }
        
        return distances;
    }
};

}

#endif /* BFSShortestReachInGraph_hpp */
