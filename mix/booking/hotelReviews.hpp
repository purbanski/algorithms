//
//  hotelReviews.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 17/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef hotelReviews_hpp
#define hotelReviews_hpp

#include <stdio.h>

namespace HotelReviews {
    void solution();
}
#endif /* hotelReviews_hpp */
