//
//  hotelReviews.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 17/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "hotelReviews.hpp"
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <queue>

using namespace std;

namespace HotelReviews {
    
    struct greater {
        template<class T>
        bool operator()(const T& t1, const T& t2) {
            return t1 > t2;
        }
    };
    
    void solution() {
        
        stringstream input;
        std::ifstream ifs ("/Users/przemek/code/codility/codility-lessons-again/codility-lessons-refresh/codility-lessons-refresh/booking/hotelReviews.txt", std::ifstream::out);
        
        if (ifs) {
            input << ifs.rdbuf();
            ifs.close();
        }
        
        auto convertStringToLowerCase = []( string &str ) -> void {
            std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        };
        
        string line;
        getline(input, line);
        istringstream issLine(line);
        
        string searchWord;
        set<string> rankingWords;
        
        while ( issLine >> searchWord ) {
            convertStringToLowerCase(searchWord);
            rankingWords.insert(searchWord);
        }
        
        int reviewsCount;
        input >> ws >> reviewsCount;
        
        int hotelId;
        string hotelReview;
        
        map<int, int> hotelRanking;
        
        for (int i=0; i<reviewsCount; i++) {

            input >> hotelId >> ws;
            getline(input, line);
            istringstream review(line);
            string reviewWord;
            
            auto removeCharFromString = [] (string &str, const char* charsToRemove) {
                int len = strlen(charsToRemove);
                for (int i=0; i<len; i++ ) {
                    char c = charsToRemove[i];
                    str.erase( std::remove(str.begin(), str.end(), c));
                }
            };
            
            while ( review >> reviewWord ) {
                removeCharFromString( reviewWord, ".," );
                convertStringToLowerCase(reviewWord);
                if ( rankingWords.find(reviewWord) != rankingWords.end() ) {
                    if ( hotelRanking.find(hotelId) == hotelRanking.end()) {
                        hotelRanking[hotelId] = 1;
                    }
                    else {
                        hotelRanking[hotelId]++;
                    }
                }
            }
        }
        
        using IntPair = pair<int, int>;
        
        vector<IntPair> hotelRankingSorted;
        for ( auto e : hotelRanking ) {
            hotelRankingSorted.push_back(IntPair(e.second, e.first));
        }

        std::sort(hotelRankingSorted.begin(), hotelRankingSorted.end(), greater());
        for ( auto e : hotelRankingSorted) {
            cout << e.second << " ";
        }
    }
}
