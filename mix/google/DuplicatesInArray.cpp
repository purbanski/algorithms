//
//  DuplicatesInArray.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 17/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "DuplicatesInArray.hpp"
// Time: O(n)
// Space: O(1)
// It will not work if there are values in array < 0
// 0 case is handled fine

namespace DuplicatesInArray {
    std::vector<int> duplicatesInArray(std::vector<int> &A)
    {
        std::vector<int> ret;
        int processedMarker = A.size();
        
        for (int i=0; i<A.size(); i++) {
            if ( A[i] != i ) {
                int newIndex;
                
                while( A[i] != i ) {
                    newIndex = A[i];
                    if (A[i] == processedMarker || A[newIndex] == processedMarker )
                        break;
                    
                    if (A[newIndex] == A[i]) {
                        ret.push_back(A[i]);
                        A[newIndex] = processedMarker;
                        A[i] = processedMarker;
                        break;
                    }
                    
                    std::swap(A[i], A[newIndex]);
                }
            }
        }
        return ret;
    }
}
