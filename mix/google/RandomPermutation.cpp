//
//  RandomPermutation.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 16/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "RandomPermutation.hpp"
#include <random>
#include <vector>


using namespace std;

namespace RandomPermutation {
    
    float getRandom() {
        double randValue = rand();
        return randValue / RAND_MAX;
    }
    
    int getRandomInt(int max) {
        return getRandom() * max;
    }
    
    vector<int> getRandomPermutation(int n) {
        vector<int> permutation(n,-1);
        for (int i=0; i<n; i++) {
            permutation[i] = i+1;
        }
        
        int temp;
        for (int i=0; i<n; i++) {
            int shuffleIndex = getRandomInt(n);
            
            temp = permutation[i];
            permutation[i] = permutation[shuffleIndex];
            permutation[shuffleIndex] = temp;
        }
        return permutation;
    }
}
