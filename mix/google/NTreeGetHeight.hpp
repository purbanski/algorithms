//
//  NTreeGetHeight.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 17/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef NTreeGetHeight_hpp
#define NTreeGetHeight_hpp

#include <stdio.h>
#include <list>
#include <vector>

using namespace std;

namespace NTreeGetHeight {
    class Graph {
    public:
        Graph(int elements);
        void addEdge(int parent, int child);
        int height();
    
    private:
        int travers(int nodeId, int nodeHeight);
        
    private:
        vector< list<int> > _nodes;
        int _height;
    };
};
#endif /* NTreeGetHeight_hpp */
