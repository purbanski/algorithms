//
//  NTreeGetHeight.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 17/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "NTreeGetHeight.hpp"
#include <iostream>

using namespace std;

namespace NTreeGetHeight {
    
    Graph::Graph(int elements) {
        _nodes.resize(elements);
        _height = 0;
    }
    
    void Graph::addEdge(int parent, int child) {
        _nodes[parent].push_front(child);
    }
    
    int Graph::height() {
        // for graph with nodes count 0, 1, 2
        // its height is equal to its node cound.
        if (_nodes.size() < 3 )
            return _nodes.size();
        
        travers(0, 0);
        return _height;
    }
    
    int Graph::travers(int nodeId, int nodeHeight) {
        for (auto node : _nodes[nodeId] ) {
            _height = max( travers(node, nodeHeight+1), _height);
        }
        
        // There is a bit of optimization can be done here.
        // Keep track of visited node count.
        // Then if ( total_graph_nodes_count - visited_nodes_count <= _height )
        // you can stop traversing. As there is not enough unvisited nodes which can beat
        // current _height;
        return nodeHeight;
    }
};
