//
//  RootToLeafPath.hpp
//

#ifndef RootToLeafPath_hpp
#define RootToLeafPath_hpp

#include <stdio.h>
#include <vector>
#include <iostream>
#include <list>
#include <sstream>

using namespace std;

namespace RootToLeafPath {
    class TreeNode {
        
    public:
        TreeNode( int val ) {
            _value = val;
            _left = _right = NULL;
        }
        
        ~TreeNode() {
            // free nodes memory
        }
        
        TreeNode* addLeft(int value) {
            TreeNode* node = new TreeNode(value);
            _left = node;
            return _left;
        }

        TreeNode* addRight(int value) {
            TreeNode* node = new TreeNode(value);
            _right = node;
            return _right;
        }
        
        bool amILeaf() const {
            return (_right == NULL && _left == NULL);
        }
        
        vector<string> pathsToLeafs() {
            _paths.clear();

            list<int> path;
            travers(this, path);
            return _paths;
        }
        
    private:
        void travers(TreeNode* node, list<int> path) {
            if ( node == NULL ) {
                return;
            }

            path.push_back(node->_value);
            
            if (node->amILeaf()) {
                ostringstream pathSS;
                
                for (auto p : path) {
                    pathSS << p << "->";
                }
                string pathString;
                pathString = pathSS.str();
                pathString.erase(pathString.end()-2, pathString.end());
                
                _paths.push_back(pathString);
            }
            else {
                travers(node->_left, path);
                travers(node->_right, path);
            }
        }
        
    private:
        static vector<string> _paths;
        int _value;
        TreeNode *_left, *_right;
    };
}


#endif /* RootToLeafPath_hpp */
