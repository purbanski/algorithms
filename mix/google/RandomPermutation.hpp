//
//  RandomPermutation.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 16/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef RandomPermutation_hpp
#define RandomPermutation_hpp

#include <stdio.h>
#include <vector>

using namespace std;

namespace RandomPermutation {
    
    float getRandom();
    int getRandomInt(int max);
    vector<int> getRandomPermutation(int n);
}

#endif /* RandomPermutation_hpp */
