//
//  DuplicatesInArray.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 17/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef DuplicatesInArray_hpp
#define DuplicatesInArray_hpp

#include <stdio.h>
#include <vector>

namespace DuplicatesInArray {
    std::vector<int> duplicatesInArray(std::vector<int> &A);
}

#endif /* DuplicatesInArray_hpp */
