//
//  fib.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 04/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>

namespace fibonacci {
    int fib( int i ) {
     
        if (i == 1 || i == 2 ) return 1;
        
        
        return (fib(i-2)+fib(i-1));
    }
}