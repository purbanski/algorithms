//
//  l1-binary-gap.hpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 03/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef l1_binary_gap_hpp
#define l1_binary_gap_hpp

#include <stdio.h>
#include <vector>


namespace fibonacci {
    int fib( int i );
}

namespace binary_gap {
    int solution(int n);
}

namespace OddOccurrencesInArray {
    int solution( std::vector<int> &A);
}

namespace CyclicRotation {
    std::vector<int> solution(std::vector<int> &A, int K);
}

namespace TapeEquilibrium {
    int solution(std::vector<int> &A);
}

namespace FrogJump {
    int solution(int X, int Y, int D);
}

namespace PermMissingElem {
    int solution(std::vector<int> &A);
}

namespace PermCheck {
    int solution(std::vector<int> &A);
}

namespace FrogRiverOne {
    int solution(int X, std::vector<int> &A);
}

namespace MissingInteger {
    int solution(std::vector<int> &A);
}

namespace MaxCounters {
    std::vector<int> solution(int N, std::vector<int> &A);
}

namespace PassingCars {
    int solution(std::vector<int> &A);
}

namespace CountDiv {
    int solution(int A, int B, int K);
}


namespace MinAvgTwoSlice {
    int solution(std::vector<int> &A);
}

namespace GenomicRangeQuery {
    std::vector<int> solution(std::string &S,
                              std::vector<int> &P,
                              std::vector<int> &Q);
}

namespace MaxProductOfThree {
    int solution(std::vector<int> &A);
}

namespace Distinct {
    int solution(std::vector<int> &A);
}

namespace Fish {
    int solution(std::vector<int> &A, std::vector<int> &B);
}

namespace NumberOfDiscIntersections {
    int solutionWithSort(std::vector<int> &A);
    int solutionGenius(std::vector<int> &A);
    int solutionGeniusVar2(std::vector<int> &A);
    int solutionBruteForce(std::vector<int> &A);
}

#endif /* l1_binary_gap_hpp */
