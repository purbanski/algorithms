//
//  main.cpp
//  codility-challenges
//
//  Created by Przemek Urbanski on 17/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//
// Codility: https://codility.com/programmers/task/trek_and_swim/
// Argon 2015

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

class LeaderMap : public map<int,int> {
public:
    LeaderMap( int value, const vector<int> &vec )
    {
        _dataSize = vec.size();
        
        int level = 0;
        for (int i = 0; i<vec.size()-1; i++)
        {
            if (vec.at(i) == value )
                level++;
            else
                level--;
            
            if (vec.at(i) == value && vec.at(i+1) != value )
                (*this)[i]=level;
        }
    }

    void dump() {
        for( auto p :  *this)
                cout << "index: "<< p.first << " level: " << p.second << endl;
            cout << "--------------"<< endl;
    };
    
    int match(const LeaderMap& matchMap ) {
        int maxSlice = 0;
        int rid;
        
        for (auto p : *this )
        {
            rid = _dataSize - p.first - 2;
            auto rp = matchMap.find(rid);
            
            if ( rp != matchMap.end())
            {
                int level = 0 ;
                if ( p.second < 1 )
                    level -= (1-p.second);
                
                if ( rp->second < 1 )
                    level -= (1-rp->second);
                
                maxSlice = std::max(maxSlice, _dataSize+level);
                
                // max reached
                // we can exit
                if (maxSlice == _dataSize)
                    return maxSlice;
            }
        }
        return maxSlice;
    };
    
private:
    int _dataSize;
};

//-----------------------------
// solution
//-----------------------------
int solution(vector<int> &A) {
    vector<int> revA = A;
    std::reverse(revA.begin(), revA.end());
    
    LeaderMap lr(0, A);
    LeaderMap rl(1, revA);
    
//    lr.dump();
//    rl.dump();
    
    return lr.match(rl);
}

//int main(int argc, const char * argv[]) {
//    vector<int> a;
//    
//    a={1, 1, 0, 1, 0, 0, 1, 1}; // 7
//    std::cout << solution(a)<< endl;
//    
//    a={1, 0}; // 0
//    std::cout << solution(a)<< endl;
//    
//    a={0,1,0,1}; // 4
//    std::cout << solution(a)<< endl;
//    
//    a={1,0,1,0,1}; // 4
//    std::cout << solution(a)<< endl;
//    
//    a={1,0,1,0,1,0}; // 4
//    std::cout << solution(a)<< endl;
//    
//    a={0,1,0,1,0}; //4
//    std::cout << solution(a)<< endl;
//    
//    a = {1,0,0,1}; //4
//    std::cout << solution(a)<< endl;
//    
//    a = {1,1,1,1,1,1,0,1,0,1}; //4
//    std::cout << solution(a)<< endl;
//    
//    
//    return 0;
//}
