//
//  l2-CyclicRotation.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 03/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//
//

#include <stdio.h>
#include <vector>
#include <iostream>

using namespace std;

namespace CyclicRotation {
   
   vector<int> solution(vector<int> &A, int K) {

       if ( ! A.size())
           return A;
       
       while (K>A.size()){
           K -= A.size();
       }
       
       vector<int> ret;
       ret.insert(ret.begin(), A.begin(), A.end()-K);
       ret.insert(ret.begin(), A.end()-K, A.end());
      
       return ret;
    };
}

