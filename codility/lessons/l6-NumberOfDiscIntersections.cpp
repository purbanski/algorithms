//
//  l6-NumberOfDiscIntersections.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 17/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <iostream>
#include <map>
#include <queue>

using namespace std;

namespace NumberOfDiscIntersections {
    
    // Codility task:
    // https://codility.com/programmers/lessons/6-sorting/number_of_disc_intersections/
    //
    // Orginal solution by Tolja (fantastic job!):
    // http://stackoverflow.com/questions/4801242/algorithm-to-calculate-number-of-intersecting-discs
    //
    // Tolja:
    // http://stackoverflow.com/users/1865826/%D0%A2%D0%BE%D0%BB%D1%8F
    //
    // Complexity O(n)
    // Space O(n)
    // Codility score: 100% / 100%
    //
    // Below is Tolja solution, in C++, with comments and explanations.
    
    int solutionGenius(vector<int> &A) {
        
        int intersectionCount = 0;
        
        vector<int> diskStartPointCount(A.size());
        vector<int> diskEndPointCount(A.size());

        // Calculate 'normalized' start and end point for each disk.
        // By normalize I mean, start and end point of a disk must
        // be able to act as an index in array of A.size() [ we are
        // going to use theses start and end points as indexes in
        // diskStartPointCount and diskEndPointCount arrays ]. Therefore
        // if start point of any disk is less than 0, set it as 0.
        // If end point of any disk is greater than A.size()-1,
        // set it as A.size()-1. Chopping this disk/intervals as
        // described above does not reduce number of intersections.
        //
        
        // For example if input is {1, 5, 2, 1, 4, 0};

        // Before normalization
        //
        // disk5                                       *
        // disk4                   |---------------*---------------|
        // disk3                           |---*---|
        // disk2                   |-------*-------|
        // disk1   |-------------------*-------------------|
        // disk0               |---*---|
        //
        //    -|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---
        //        -4  -3  -2  -1   0   1   2   3   4   5   6   7   8   9
        //
        //====
        //
        // After normalization
        //
        //                      min index         max index
        //                         ^                   ^
        // disk5                   ^                   *
        // disk4                   ^---------------*---^
        // disk3                   ^       |---*---|   ^
        // disk2                   ^-------*-------|   ^
        // disk1                   ^---*---------------^
        // disk0                   *---|               ^
        //                         ^                   ^
        //                       --|---|---|---|---|---|--
        //                         0   1   2   3   4   5
        
        int leftLimit = 0;
        int rightLimit = (int)A.size() - 1;
        
        for (long i = 0; i < A.size(); i++)
        {
            long startPoint = ( i - A[i] < leftLimit  ) ? leftLimit  : ( i - A[i] );
            long endPoint   = ( i + A[i] > rightLimit ) ? rightLimit : ( i + A[i] );
            
            diskStartPointCount[startPoint]++;
            diskEndPointCount[endPoint]++;
        }
        
        int activeDisksCount = 0;
        const int maxIntersectionCount = 10000000;
        
        for (int i = 0; i < A.size(); i++)
        {
            if (diskStartPointCount[i] > 0)
            {
                // If any disk are starting at this point calculate all
                // all intersection for them according to formula :
                // n*(n-1)/2 [ where n euals number of disks starting at
                // this point]
                //
                // Example if 4 disk are started at this point,
                // let say disk 1, 2, 3, 4 then intersections are:
                // 1+2, 1+3, 1+4, 2+3, 2+4, 3+4
                // so intersections count is n*(n-1)/2
                intersectionCount += diskStartPointCount[i] * (diskStartPointCount[i] - 1) / 2;

                // Variable activeDisksCount tracks count of
                // disks which have already started but
                // did not finished. So increase number of intersections
                // by disk which have started in previous iteration
                // multiply by disks which has started in this operation.
                intersectionCount += activeDisksCount * diskStartPointCount[i];
                
                // Increase activeDisksCount by amount of disks which
                // have started in this interation.
                activeDisksCount += diskStartPointCount[i];
            }
            
            // If there are any end points for this index/iteration - it means
            // disk(s) have stopped here. Therefore amount of activeDisksCount
            // has to be decresed by amount of this which has stopped in this iteration.
            activeDisksCount -= diskEndPointCount[i];
            
            if ( maxIntersectionCount < intersectionCount ) return -1;
        }
        
        return intersectionCount;
    }
    
    //-------------------------
    
    int solutionGeniusVar2(vector<int> &A) {
        
        int intersectionCount = 0;
        
        queue<int> diskStartPointCount;
        queue<int> diskEndPointCount;
        
        int leftLimit = 0;
        int rightLimit = (int)A.size() - 1;
        
        for (long i = 0; i < A.size(); i++)
        {
            long startPoint = i - A[i];
            long endPoint   = i + A[i];
            
            diskStartPointCount.push(startPoint);
            diskEndPointCount.push(endPoint);
        }
        
        int activeDisksCount = 0;
        const int maxIntersectionCount = 10000000;
        
//        while ( ! diskStartPointCount.empty() ) {
//            int s = diskStartPointCount.pop();
//            int e = diskEndPointCount.pop();
//        }
//        for (int i = 0; i < A.size(); i++)
//        {
//            if (diskStartPointCount[i] > 0)
//            {
//                intersectionCount += diskStartPointCount[i] * (diskStartPointCount[i] - 1) / 2;
//                
//                // Variable activeDisksCount tracks count of
//                // disks which have already started but
//                // did not finished. So increase number of intersections
//                // by disk which have started in previous iteration
//                // multiply by disks which has started in this operation.
//                intersectionCount += activeDisksCount * diskStartPointCount[i];
//                
//                // Increase activeDisksCount by amount of disks which
//                // have started in this interation.
//                activeDisksCount += diskStartPointCount[i];
//            }
//            
//            // If there are any end points for this index/iteration - it means
//            // disk(s) have stopped here. Therefore amount of activeDisksCount
//            // has to be decresed by amount of this which has stopped in this iteration.
//            activeDisksCount -= diskEndPointCount[i];
//            
//            if ( maxIntersectionCount < intersectionCount ) return -1;
//        }
        
        return intersectionCount;
    }
    
    //------------------------------------------
    
    int solutionBruteForce(vector<int> &A) {
    
        int intersectionCount = 0;
        for (int i=0; i<A.size()-1; i++) {
            for (int j=i+1; j<A.size(); j++) {
//                cout << i << " " << j << endl;
                
                int aBegin = i - A[i];
                int aEnd   = i + A[i];

                int bBegin = j - A[j];
                int bEnd   = j + A[j];
                
                if ( ! ( aEnd < bBegin || aBegin > bEnd) ) {
                    intersectionCount++;
                }
            }
        }
        return intersectionCount;
    }
    
    //-------------------------

    int solutionWithSort(vector<int> &A) {
        
        vector< pair<long, long> > B(A.size());
        
        for ( int i=0; i< A.size(); i++ ) {
            B[i].first  = (long)i-A[i];
            B[i].second = (long)A[i] +i;
        }
        
        sort(B.begin(), B.end());
        
        int intersectionCount = 0;
        
        for (int i=0; i<B.size(); i++) {
            int low = i+1;
            int high = B.size() - 1;
            int mid = (low+high)/2;
            long cmp = B[i].second;
            
            while(low<=high) {
                
                if ( cmp >= B[mid].first ) {
                    low = mid + 1;
                } else {
                    high = mid - 1;
                }
                mid = (low+high)/2;

            }
            intersectionCount += (mid -i);
            if ( intersectionCount > 10000000 ) {
                return -1;
            }
        }
        return intersectionCount;
    }
    
    //-------------------------

}
