//
//  l2-OddOccurrencesInArray.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 03/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <iostream>

using namespace std;

namespace OddOccurrencesInArray {
    int solution( std::vector<int> &A ) {

        int ret = 0;
        for (auto it = A.begin(); it != A.end(); it++) {
            ret = ret ^ *it;
        }
        return ret;
    };
}

