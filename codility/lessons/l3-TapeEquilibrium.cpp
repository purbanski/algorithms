//
//  l3-TapeEquilibrium.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 04/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include "all.hpp"
#include <vector>
#include <iostream>
#include <limits>

using namespace std;

namespace TapeEquilibrium {
    int solution(vector<int> &A) {
        
        int r, l, min, diff;
        r = l = min = diff = 0;
        
        for ( int e : A) {
            r += e;
        }
        
        l += A[0];
        r -= A[0];
        min = abs(l-r);
        
        for ( unsigned int i = 1; i<A.size()-1; i++ ) {
            l += A[i];
            r -= A[i];
            diff = abs(l-r);
            min = std::min(min, diff);
        }
        
        return min;
    }
}
