//
//  l3-PermMissingElem.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 04/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//


#include <stdio.h>
#include "all.hpp"
#include <iostream>
using namespace std;

namespace PermMissingElem {
    int solution(vector<int> &A) {
        
        int vsize = (int) A.size();
        long esum = (1L + vsize + 1L) * (vsize+1L) /(2L); // watch out here .. 
        long asum = 0;
        for (int a : A ) {
            asum += a;
        }
        
        return (int)(esum-asum);
    }
}