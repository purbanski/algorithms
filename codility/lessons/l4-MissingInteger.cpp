//
//  l4-MissingInteger.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 05/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

namespace MissingInteger {
    int solution(vector<int> &A) {
        
        std::vector<int> counter(A.size()+1, 0);
        
        for (auto a: A) {
            if ( a>0 && a<counter.size() ) {
                counter[a] = 1;
            }
        }
        
        for ( int i=1; i< counter.size(); i++ ) {
            if ( ! counter[i] )
                return i;
        }
        
        return (int)counter.size();
    }
}