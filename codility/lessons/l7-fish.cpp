//
//  l7-fish.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 14/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include "all.hpp"
#include <stack>

using namespace std;

namespace Fish {
    int solution(vector<int> &fishSizes, vector<int> &fishDirections) {

        class Fish {
        public:
            Fish(int size, int direction) {
                _size = size;
                _direction = direction;
            }
            
            int size() const {
                return _size;
            }
            
            int direction() const {
                return _direction;
            }
        private:
            int _size;
            int _direction;
        };
        
        //-----------
        
        int fishSurvived = 0;
        stack<Fish> fishStack;
        
        for (int i = 0; i < fishSizes.size(); i++ ) {
            Fish fish = Fish(fishSizes[i], fishDirections[i]);
            
            if ( fishStack.size() == 0) {
                if ( fish.direction() == 0 ) {
                    fishSurvived++;
                } else {
                    fishStack.push(fish);
                }
            } else {
                if (fish.direction() == 1 ) {
                    fishStack.push(fish);
                } else {
                    // fish is zero
                    while(fishStack.size()) {
                        Fish fishTop = fishStack.top();
                        if ( fishTop.size() < fish.size() ) {
                            fishStack.pop();
                            if (fishStack.size() == 0) {
                                fishSurvived++;
                            }
                        } else {
                            // zjedzona nowa
                            break;
                        }
                    }
                }
            }
            
        }
        fishSurvived += fishStack.size();
        
        return fishSurvived;
        
    }
}
