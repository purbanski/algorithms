//
//  l3-FrogJump.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 04/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include "all.hpp"
#include <iostream>
using namespace std;

namespace FrogJump {
    int solution(int X, int Y, int D) {
        int distance = Y - X;
        int steps =  ceil((double)distance/(double)D);
        return ceil(steps);
    }
}