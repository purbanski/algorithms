//
//  l5-CountDiv.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 07/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

namespace CountDiv {
    int solution(int A, int B, int K) {
        return (B/K)-(A/K) + !(A%K);
    }
}