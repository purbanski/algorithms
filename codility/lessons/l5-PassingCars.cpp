//
//  l5-PassingCars.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 07/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;


namespace PassingCars {
    int solution(vector<int> &A) {
        int zeroCount = 0;
        int sum = 0;
        
        for (int a : A ) {
            if ( a == 0 ) {
                zeroCount++;
            } else {
                sum += zeroCount;
                if (sum > 1000000000)
                    return -1;
            }
        }
        
        return sum;
    }
}