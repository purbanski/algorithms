//
//  l1-binary-gap.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 03/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <cmath>
#include <iostream>

#include "all.hpp"
using namespace std;

namespace binary_gap {
    int solution(int N) {
        int count = 0;
        int max = 0;
        
        while ( N % 2 == 0 ) {
            N >>= 1;
        }
        
        while (N) {
            if ( N % 2 == 0 ) {
                // last was zero
                count++;
                max = std::max(max, count);
            } else {
                // last was one
                count = 0;
            }
            
            N >>= 1;
        }
        return max;
    }
}