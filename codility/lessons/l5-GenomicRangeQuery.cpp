//
//  l5-GenomicRangeQuery.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 07/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <map>

#include <iostream>

using namespace std;

namespace GenomicRangeQuery {
    vector<int> solution(string &S,
                         vector<int> &P,
                         vector<int> &Q) {
        
        // Split AGCT sequecne so for every nucloid
        // there is a vector representing existance
        // ex :
        // ACCAT
        // V(A) = 1,0,0,1,0
        // V(C) = 0,1,1,0,0
        // V(G) = 0,0,0,0,0
        // V(T) = 0,0,0,0,1
        //
        // Then calculate prefix sum for each vector.
        // Based on the prefix sum you can find out if in the given
        // sequence nucloid exists - and you are done :)
        
        typedef vector<int> VecInt;
        typedef map<char, VecInt > NucMap;
        
        NucMap nucMap;
        nucMap['A'] = VecInt(S.size(),0);
        nucMap['C'] = VecInt(S.size(),0);
        nucMap['G'] = VecInt(S.size(),0);
        nucMap['T'] = VecInt(S.size(),0);
        
        for (int i=0; i < S.size(); i++ ) {
            VecInt *pv;
            pv = &nucMap[ S[i] ];
            pv->at(i) = 1;
        }
        
//        cout << "-----------------" << endl;
//        cout << "Occurances " << endl;
//        
//        for (auto ele : nmap) {
//            cout << ele.first << ": ";
//            for ( auto e : ele.second ) {
//                cout << e << ", ";
//            }
//            cout << endl;
//        }
        
        NucMap sumMap;
        sumMap['A'] = VecInt(S.size(),0);
        sumMap['C'] = VecInt(S.size(),0);
        sumMap['G'] = VecInt(S.size(),0);
        sumMap['T'] = VecInt(S.size(),0);
        
        for (auto nuc : nucMap ) {
            VecInt *dst, *src;
            
            src = &nucMap[ nuc.first ];
            dst = &sumMap[ nuc.first ];
            
            dst->at(0) = src->at(0);
            for ( int i=1; i<src->size(); i++) {
                dst->at(i) = dst->at(i-1) + src->at(i);
            }
        }

//        cout << "-----------------" << endl;
//        cout << "Sum " << endl;
//        for (auto ele : sumMap) {
//            cout << ele.first << ": ";
//            for ( auto e : ele.second ) {
//                cout << e << ", ";
//            }
//            cout << endl;
//        }
        
        auto impactFactor = [](char c) -> int {
            int ret = -1;
            switch (c) {
                case 'A': ret = 1; break;
                case 'C': ret = 2; break;
                case 'G': ret = 3; break;
                case 'T': ret = 4; break;
                default: break;
            }
            return ret;
        };

        VecInt ret(P.size(), 0);
        for (int i = 0; i<P.size(); i++) {
            int start = P[i];
            int stop  = Q[i];
            
            string dna("ACGT");
            for (auto c : dna) {
                VecInt *sums;
                sums = &sumMap[ c ];
                
                int occurance = 0;
                if ( start > 0 ) {
                    occurance = sums->at(stop) - sums->at(start-1);
                } else {
                    occurance = sums->at(stop);
                }
                
                if (occurance != 0 ) {
                    ret.at(i) = impactFactor( c );
                    break;
                }
            }
        }
        return ret;
    }
}