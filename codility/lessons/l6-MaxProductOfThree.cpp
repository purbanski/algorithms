//
//  l6-MaxProductOfThree.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 11/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "all.hpp"

using namespace std;

namespace MaxProductOfThree {
    int solution(vector<int> &A) {
        std::sort(A.begin(), A.end());
        
        return max(A[0]*A[1]*A[A.size()-1], A[A.size()-3]*A[A.size()-2]*A[A.size()-1]);
    }
}
