//
//  l4-MaxCounters.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 05/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <iostream>

using namespace std;

namespace MaxCounters {
    vector<int> solution(int N, vector<int> &A) {
        
        vector<int> counter(N, 0);
        int max_value = 0;
        int max_used = 0;
        
        for (int i=0; i<A.size(); i++) {
            int ind = A[i];
            
            if (ind == (N+1)) {
                max_used = max_value;
            } else {
                counter[ind-1] = max(counter[ind-1], max_used);
                counter[ind-1]++;
                max_value = max (max_value, counter[ind-1]);
            }
        }
        
        for (int &c : counter) {
            c = max(c, max_used);
        }

        return counter;
    }
}