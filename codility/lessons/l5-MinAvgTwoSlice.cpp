//
//  l5-MinAvgTwoSlice.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 07/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

namespace MinAvgTwoSlice {
    int solution(vector<int> &A) {
        
        vector<int> psum(A.size(), 0 );
        
        psum[0] = A[0];
        for (int i=1; i<A.size(); i++ ) {
            psum[i] = psum[i-1]+A[i];
        }
        
        unsigned int index = 0;
        float minAvg = 0;
        float avg2 = static_cast<float>(psum[1]) / 2.0f;
        float avg3 = static_cast<float>(psum[2]) / 3.0f;
        
        minAvg = std::min(avg2, avg3);

        for (int i=1; i<A.size()-2; i++) {
            avg2 = static_cast<float>(psum[i+1] - psum[i-1]) / 2.0f;
            avg3 = static_cast<float>(psum[i+2] - psum[i-1]) / 3.0f;
            if ( minAvg > avg2 || minAvg > avg3 ) {
                index = i;
                minAvg = std::min(avg2, avg3 );
            }
        }
        
        avg2 = static_cast<float>(psum[psum.size()-1] - psum[psum.size()-3]) / 2.0f;

        if ( minAvg > avg2 ) {
            index = (unsigned int)psum.size()-2;
        }
        minAvg = std::min(avg2, avg3);

            
        return index;
    }
}