//
//  l4-PermCheck.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 05/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <iostream>

using namespace std;

namespace PermCheck {
    int solution(std::vector<int> &A) {

        vector<int> counter(A.size()+1, 0);
        int size = (int) counter.size();
        
        for (int a: A) {
            if ( a >= size )
                return 0;
            
            if ( a < 1 )
                return 0;
            
            counter[a]++;
        }
        
        for (unsigned int i=1; i<counter.size(); i++) {
            if (counter[i] != 1 ) {
                return 0;
            }
        }

        return 1;
    }
}