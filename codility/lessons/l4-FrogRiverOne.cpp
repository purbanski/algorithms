//
//  l4-FrogRiverOne.cpp
//  codility-lessons-refresh
//
//  Created by Przemek Urbanski on 05/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <iostream>

using namespace std;

namespace FrogRiverOne {
    int solution(int X, vector<int> &A) {
        vector<int> counter(X+1, -1);
        
        for (int i=0; i<A.size(); i++) {
            int a = A[i];

            if (counter[a] == -1 ) {
                counter[a] = i;
            }
        }

        int ret = counter[0];
        for ( int i=1; i<counter.size(); i++ ) {
            if (counter[i] == -1)
                return -1;
            ret = std::max(ret, counter[i]);
        }
        return ret;
    }
}