//
//  lesson4.cpp
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#include "lesson4.h"

#include <algorithm>
#include <iostream>
#include <set>

//------------------------------
// MaxProductOfThree
//------------------------------
namespace MaxProductOfThree
{
    int solution(vector<int> &A)
    {
        std::vector<int> v = A;
        std::make_heap (v.begin(),v.end());
        std::sort_heap (v.begin(),v.end());
        
        unsigned long s = v.size();

        int ls = v[0]*v[1]*v[s-1];
        int rs = v[s-1]*v[s-2]*v[s-3];
        
        if (ls>rs)
            return ls;
        else
            return rs;
    }
}

//------------------------------
// Distinct
//------------------------------
namespace Distinct {
    int solution_a(vector<int> &A)
    {
        std::set<int> s;
        for (auto i : A )
            s.insert(i);
        return (int)s.size();
    }

    int solution_b(vector<int> &A)
    {
        if (!A.size())
            return 0;
        
        std::make_heap(A.begin(), A.end());
        std::sort_heap(A.begin(), A.end());
        
        int count = 1;
        int last = A[0];
        for (auto i : A )
        {
            if (i!=last)
                count++;
            last = i;
        }
        return count;
    }
}

//------------------------------
// Traingle
//------------------------------
namespace Traingle {
    int solution(vector<int> &A) {
        if (A.size()<3)
            return 0;
        
        std::make_heap(A.begin(), A.end());
        std::sort_heap(A.begin(), A.end());
        
        for (int i = 0; i<A.size()-2; i++)
        {
            if ((long long)A[i]+(long long)A[i+1]>A[i+2])
                return 1;
        }
        
        return 0;
    }
}

namespace NumberOfDiscIntersections {
    
    int binSearch(vector<int> &v, int element)
    {
        if (v.size()==0)
            return -1;
        
        int s = v.size();
        int h = s  /2;

        int ret = -1;
        
        if ( v.at(h) == element )
            return h;
        
        std::vector<int> vl (v.begin(), v.begin()+h);
        std::vector<int> vr (v.begin()+h, v.end());

        int lret = binSearch(vl, element);
        if (lret!=-1)
            return lret;
        
        int rret = binSearch(vr, element);
        if (rret!=-1)
            return rret;
        
//        for (auto i:vl)
//            std::cout << i <<", ";
//
//        std::cout << "\n------------\n";
//
//        for (auto i:vr)
//            std::cout << i <<", ";
//        
       
        return -1;
        
//        binSearch(vector<int>, {)
    }
    
    int solution(vector<int> &A) {
        std::make_heap(A.begin(), A.end());
        std::sort_heap(A.begin(), A.end());
        
        binSearch(A,10);
        return 1;
    }
}
