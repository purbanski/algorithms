//
//  lesson3.h
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#ifndef __codility_frog_jump__lesson3__
#define __codility_frog_jump__lesson3__

#include <stdio.h>
#include <vector>

using namespace std;

namespace PassingCars {
    int solution(vector<int> &A);
}

namespace MinAvgTwoSlice {
    int solution(vector<int> &A);
}

namespace CountDiv {
    int solution(int A, int B, int K);
}

namespace GenomicRangeQuery {
    vector<int> solution(string &S, vector<int> &P, vector<int> &Q);
}

#endif /* defined(__codility_frog_jump__lesson3__) */
