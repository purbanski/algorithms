//
//  lesson1.cpp
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#include "lesson1.h"
#include <cstdlib>

//----------------
// Lesson 1
//----------------
namespace frogjump {
    int solution(int X, int Y, int D) {
        
        int d = Y - X;
        int c = d/D;
        
        if (c*D<d)
            c++;
        return c;
    }
}

namespace PermMissingElem {
    int solution(int A[], int N) {
        
        long s = (1L + (N+1))*(N+1)/2;
        long sum = 0;
        
        for (int i=0; i<N; i++ )
            sum += A[i];
        
        return (int)(s - sum);
    }
}

namespace TapeEquilibrium {
    
    int solution(int A[], int N) {
        int l = 0;
        int r =0;
        int min = 0;
        l = A[0];
        for (int i=1; i<N; i++)
            r += A[i];
        
        min = std::abs(l -r);
        
        for (int i=1; i<N-1; i++)
        {
            l = l + A[i];
            r = r - A[i];
            
            int d = abs(l-r);
            
            if (d<min)
                min=d;
        }
        
        return min;
    }
}
