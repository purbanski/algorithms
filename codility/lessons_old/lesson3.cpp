//
//  lesson3.cpp
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#include "lesson3.h"
#include <string>

using namespace std;

//----------------
// Lesson 3
//----------------

namespace PassingCars {
    int solution(vector<int> &A) {
        int ret = 0;
        int ps = 0;
        
        for (int i=A.size()-1; i>=0; i--)
        {
            ps += A[i];
            if (!A[i])
                ret += ps;
            if (ret>1000000000)
                return -1;
        }
        
        return ret;
    }
}

namespace MinAvgTwoSlice {
    int solution(vector<int> &A) {
        int minId = 0;
        float minAvg = (A[0]+A[1])/2;
        float tAvg;
        
        for (int i=0; i<A.size()-2;i++)
        {
            
            tAvg = (A[i]+A[i+1])/2.0f;
            if (tAvg<minAvg)
            {
                minAvg = tAvg;
                minId = i;
            }
            
            tAvg = (A[i]+A[i+1]+A[i+2])/3.0f;
            if (tAvg<minAvg)
            {
                minAvg = tAvg;
                minId = i;
            }
            
        }
        
        tAvg = (A[A.size()-2]+A[A.size()-1])/2.0f;
        if (tAvg<minAvg)
        {
            minAvg = tAvg;
            minId = A.size()-2;
        }
        
        return minId;
    }
    
}


namespace CountDiv {
int solution(int A, int B, int K) {
    long long a = A;
    long long b = B;
    long long k = K;
    
    if (b==0)
        return 1;
    
    if (k>b)
        return 0;
    
    //    if (k>a)
    //        a = k;
    
    if ( a%k == 0 )
        return (int)((b-a)/k +1L);
    else
        return (int)(b-(a-a%k))/k;
}
}



//---------------
// Some problems fixme
//----------------
namespace GenomicRangeQuery {
    
    int getValue(char c)
    {
        switch (c) {
            case 'A': return 1;
            case 'C': return 2;
            case 'G': return 3;
            case 'T': return 4;
            default:
                return -1;
        }
    }
    
    vector<int> solution(string &S, vector<int> &P, vector<int> &Q) {
        std::vector<int> ret;
        
        int a[S.length()];
        int c[S.length()];
        int g[S.length()];
        int t[S.length()];
        
        int at,ct,gt,tt;
        at = ct = gt =tt =0;
        for (int i=0; i<S.length(); i++)
        {
            switch (S[i]) {
                case 'A':
                    at++;
                    break;
                case 'C':
                    ct++;
                    break;
                case 'G':
                    gt++;
                    break;
                case 'T':
                    tt++;
                    break;
                    
                default:
                    break;
            }
            a[i]=at;
            c[i]=ct;
            g[i]=gt;
            t[i]=tt;
            
        }
        
        for (int i=0; i<P.size(); i++)
        {
            if (Q[i]==P[i])
            {
                ret.push_back(getValue(S.at(Q[i])));
                continue;
            }
            int ac,cc,gc,tc = 0;
            ac = a[Q[i]] - a[P[i]];
            cc = c[Q[i]] - c[P[i]];
            gc = g[Q[i]] - g[P[i]];
            tc = t[Q[i]] - t[P[i]];
            
            if (ac) ret.push_back(1);
            else if (cc) ret.push_back(2);
            else if (gc) ret.push_back(3);
            else if (tc) ret.push_back(4);
        }
        
        return ret;
    }
}
