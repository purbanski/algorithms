//
//  lesson5.h
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#ifndef __codility_frog_jump__lesson5__
#define __codility_frog_jump__lesson5__

#include <stdio.h>
#include <string>

namespace l5 {
    int solution(std::string &S);
}
#endif /* defined(__codility_frog_jump__lesson5__) */
