//
//  lesson12.cpp
//  codility lessons
//
//  Created by Przemek Urbanski on 18/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#include "lesson12.h"
#include <iostream>
#include <vector>

using namespace std;

//------------------------------
// MinMaxDivision
//------------------------------
namespace MinMaxDivision {
    
    /*
     Key:
     
     Imagine the following game. The computer selects an integer
     value between 1 and 16 and our goal is to guess this number 
     with a minimum number of questions. For each guessed number 
     the computer states whether the guessed number is equal to, 
     bigger or smaller than the number to be guessed.
    
     The iterative check of all the successive values 1, 2,.., 16 
     is linear, because with each question the set of the candidates 
     is reduced by one.
    
     The goal is to ask a question that reduces the set of candidates 
     maximally. The best option is to choose the middle element, as 
     doing so causes the set of candidates to be halved each time. 
     With this approach, we ask the logarithmic number of questions 
     at maximum
    */
    bool check( int value, int K, vector<int> &A ) {
        int sum = 0;
        
        for (int i=0;i<A.size(); i++) {
            if ( A.at(i) > value)
                return false;
            
            sum += A.at(i);
            if ( sum > value )
            {
                sum = A.at(i);
                K--;
                if (K==0)
                    return false;
            }
        }
        return true;
    }
    
    int solution(int K, int M, vector<int> &A) {
        int sum = 0;
        int beg = 0;
        
        for (auto i:A)
        {
            sum += i;
            beg = std::max(beg,i);
        }
        
        
        int end = sum;
        int min = sum;
        
        //    cout << check(10,K,A) <<endl;
        //    cout << check(6,K,A) <<endl;
        //    cout << check(5,K,A) <<endl;
        //    cout << check(4,K,A) <<endl;
        //    cout << check(3,K,A) <<endl;
        
        while (beg<=end)
        {
            int mid = ((long long)end+(long long)beg)/2;
            
            if (check(mid,K,A))
            {
                end =mid-1;
                min = std::min(min,mid);
            }
            else
            {
                beg = mid+1;
            }
        }
        
        return min;
    }
}

//int main(int argc, const char * argv[]) {
//    
//    struct myseps : numpunct<char> {
//        /* use space as separator */
//        char do_thousands_sep() const { return ' '; }
//        
//        /* digits are grouped by 3 digits each */
//        string do_grouping() const { return "\3"; }
//    };
//    
//    std::cout.imbue(std::locale(std::locale(), new myseps));
//    
//    vector<int> A = {2,1,5,1,2,2,2};
//    cout<< MinMaxDivision::solution(3,5, A) << endl;
//
//    A={1,0,1};
//    cout<< MinMaxDivision::solution(3, 10, A) << endl;
//
//    int len = 100000;
//    A.clear();
//    for (int i=0; i<len;i++)
//        A.push_back(10000);
//
//    A.push_back(99);
//    
//    for (int i=0; i<len;i++)
//        A.push_back(0);
//    
//    return 0;
//}



#include <list>


using namespace std;
namespace NailingPlanks {
    
struct Plank {
    Plank(int aa, int bb) : a(aa), b(bb) {}
    int a;
    int b;
};

class mlist : public list<Plank> {
public:
    mlist(vector<int> &A, vector<int> &B)
    {
        for( int i=0; i<A.size(); i++)
            push_back( Plank(A.at(i), B.at(i) ));
    }
};


int solution(vector<int> &A, vector<int> &B, vector<int> &C) {
{
    int nailCount = 0;
    mlist plankList(A,B);
    
    for (int n = 0; n<C.size(); n++)
    {
        mlist::iterator p;
        p = plankList.begin();
        
        while (p != plankList.end())
        {
            if (p->a<=C.at(n) && C.at(n) <= p->b )
            {
                p = plankList.erase(p);
                if (!plankList.size())
                {
                    return n+1;
                }
            }
            else
            {
                ++p;
            }
        }
    }
    
    nailCount = nailCount==0?-1:nailCount;
    
    if (!plankList.size())
        return nailCount;
    
    return -1;
}
}
}
//
int main(int argc, const char * argv[]) {
    
    vector<int> a,b,c;
    
    a={1,1,4,5,8};
    b={4,4,5,9,10};
    c={4,6,7,10,2};
    //    c={1,1,1,1,1,1,1,1,1};
    std::cout << NailingPlanks::solution(a,b,c)<<endl;
    
    a={1};
    b={1};
    c={3};
    std::cout << NailingPlanks::solution(a,b,c)<<endl;
    //
    return 0;
}

