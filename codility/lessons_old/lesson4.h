//
//  lesson4.h
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#ifndef __codility_frog_jump__lesson4__
#define __codility_frog_jump__lesson4__

#include <stdio.h>
#include <vector>

using namespace std;

namespace MaxProductOfThree {
    int solution(vector<int> &A);
}

namespace Distinct {
    int solution_a(vector<int> &A);
    int solution_b(vector<int> &A);
}

namespace Traingle {
    int solution(vector<int> &A);
}

namespace NumberOfDiscIntersections {
    int solution(vector<int> &A);
}

#endif /* defined(__codility_frog_jump__lesson4__) */
