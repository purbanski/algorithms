//
//  lesson5.cpp
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#include "lesson5.h"
#include <stack>
#include <string>
#include <iostream>

using namespace std;

namespace l5 {
    bool match(const char l, const char r ) {
        if ( l == '{' && r=='}' ) return 1;
        if ( l == '(' && r==')' ) return 1;
        if ( l == '[' && r==']' ) return 1;
        return 0;
        
    }
    
    int solution(string &S)
    {
        if (!S.length())
            return 1;
        
        std::stack<char> stc;
        for (auto c: S)
        {
            if (c=='{' || c=='(' || c=='[')
                stc.push(c);
            else if (c=='}' || c==')' || c==']')
            {
                if (!stc.size())
                    return 0;
                
                if (!match(stc.top(),c))
                    return 0;
                stc.pop();
            }
        }
        if (!stc.size())
            return 1;
        return 0;
    }
}