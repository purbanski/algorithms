//
//  lesson2.cpp
//  codility-frog-jump
//
//  Created by Przemek Urbanski on 13/05/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#include "lesson2.h"
#include <vector>

//----------------
// Lesson 2
//----------------
namespace PermCheck {
    
    int solution(int A[], int N) {
        int ra[N];
        memset(ra,0,N*sizeof(int));
        
        long long s = (1L + N)*N/2;
        long long sum = 0;
        
        for (int i=0; i<N; i++ )
        {
            if (A[i]>N)
                return 0;
            
            sum += A[i];
            if (ra[A[i]-1]==0)
                ra[A[i]-1]=1;
            else
                return 0;
        }
        
        if (!(s - sum))
            return 1;
        
        return 0;
    }
    
}

namespace FrogRiverOne
{
    
    int solution(int X, int A[], int N) {
        int ra[N];
        memset(ra,-1,N*sizeof(int));
        
        for (int i=0; i<N; i++)
        {
            if (ra[ A[i]-1 ] == -1 )
                ra[A[i]-1] = i;
        }
        
        int m = ra[0];
        for (int i =0;i<X;i++)
        {
            if (ra[i]==-1)
                return -1;
            if (m<ra[i])
                m=ra[i];
        }
        return m;
    }
}

namespace MissingInteger {
    
    int solution(int A[], int N) {
        if (N==1)
        {
            if (A[0]!=1)
                return 1;
            return 2;
        }
        
        int ra[N];
        memset(ra,0,N*sizeof(int));
        
        for (int i=0; i<N; i++)
        {
            if (A[i]<1 || A[i]>=N)
                continue;
            ra[A[i]] = 1;
        }
        
        for (int i =1;i<N;i++)
        {
            if (ra[i]==0)
                return i;
        }
        
        return N+1;
    }
}

//C++
// performance to be improved
namespace MaxCounters {
    
    std::vector<int> solution(int N, std::vector<int> &A) {
        int max = 0;
        std::vector<int> counters;
        counters.reserve(A.size());
        
        //        for (int i=0; i<N; i++ )
        //            counters.push_back(0);
        
        for (auto id : A )
            //        for (int i = 0; i<A.size(); i++)
        {
            if (id <= N )
            {
                counters[id-1] = counters[id-1]+1;
                if (max<counters[id-1])
                    max = counters[id-1];
            }
            else
            {
                for (int j=0; j<N; j++)
                    counters[j]=max;
            }
        }
        
        return counters;
    }
}
