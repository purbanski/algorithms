#!/usr/bin/python

'''
Source : https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list
'''

l=int(raw_input())
a=raw_input().split()
a=[int(i) for i in a]

max_value = a[0]
secondmax = -100

for i in a[1:]:
    max_value = i if i>max_value else max_value

for i in a[1:]:
    secondmax = i if i>secondmax and i!=max else secondmax

print secondmax
