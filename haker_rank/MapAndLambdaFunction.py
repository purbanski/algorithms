'''
Source: https://www.hackerrank.com/challenges/map-and-lambda-expression

A list on a single line containing the cubes of the first  fibonacci numbers.

Sample Input
5

Sample Output
[0, 1, 1, 8, 27]
'''

def fib(x):
    a, b = 0, 1
    yield a
    yield b
    
    for __ in xrange(x):
        a, b = b, a+b
        yield b
        
    
l = [i**3 for i in fib(int(raw_input())-2)]
print l
