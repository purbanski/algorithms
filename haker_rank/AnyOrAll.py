'''
Source: https://www.hackerrank.com/challenges/any-or-all

You are given a space separated list of integers. If all the integers are 
positive, then you need to check if any integer is a palindromic integer.
Palindromic integer ex: 12321, 68286, 696...

Sample Input
5
12 9 61 5 14 

Sample Output
True

Can you solve this challenge in 3 lines of code or less? 
'''

__, values = raw_input(), raw_input()
print all(v>0 for v in values) and any(v == v[::-1] for v in values)
