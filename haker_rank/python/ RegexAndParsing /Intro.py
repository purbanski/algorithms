'''
Source : https://www.hackerrank.com/challenges/introduction-to-regex

> Number can start with +, - or . symbol. 
+4.50 ok 
-1.0  ok
.5    ok
-.7   ok
+.4   ok
-+4.5 not ok

> Number must contain at least  decimal value. 
12.  not ok 
12.0  ok  

> Number must have exactly one . symbol. 
> Number must not give any exceptions when converted using.

Sample Input
4  
4.0O0
-1.00
+4.54
SomeRandomStuff

Sample Output
False
True
True
False
'''

import re

for __ in xrange(int(raw_input())):
    print bool(re.search(r"^[-+]{0,1}[0-9]*\.[0-9]+$", raw_input()))
