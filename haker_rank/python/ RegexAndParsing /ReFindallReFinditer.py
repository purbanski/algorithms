'''
Source : https://www.hackerrank.com/challenges/re-findall-re-finditer
'''

import re

# s='rabcdeefgyYhFjkIoomnpOeorteeeeet'
# s='abaabaabaabaaefEAEOUOIouf'
# s='abaabaabaabaaei'
# s='bfbfbf'

s=raw_input()

v = "aeiou"
c = "qwrtypsdfghjklzxcvbnm"

a = re.findall(r'(?<=[%s])([%s]{2,})(?=[%s])' % (c, v, c), s, flags = re.I)
print '\n'.join(a or ['-1'])
