'''
Source : https://www.hackerrank.com/challenges/re-group-groups

You are given a string S. 
Your task is to find the first occurrence of an alphanumeric character in S
(read from left to right) that has consecutive repetitions.

Output Format
Print the first occurrence of the repeating character. If there are no 
repeating characters, print -1.

Sample Input
..12345678910111213141516171820212223

Sample Output
1
'''

import re
r = re.search(r'([0-9a-zA-Z])\1+', raw_input())
print (r.groups()[0] if r is not None else "-1")

