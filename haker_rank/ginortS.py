#!/usr/bin/python

'''
Source : https://www.hackerrank.com/challenges/ginorts 

Your task is to sort the string  in the following manner:
- All sorted lowercase letters are ahead of uppercase letters.
- All sorted uppercase letters are ahead of digits.
- All sorted odd digits are ahead of sorted even digits.

Sample Input
Sorting1234

Sample Output
ginortS1324
'''

from __future__ import print_function

l = sorted("Sorting1234", key=lambda x: (x.isdigit(), x in '24680', x.isupper(), x))
print (*l, sep='')