'''
Source : https://www.hackerrank.com/challenges/python-sort-sort

Sample Input
5 3
10 2 5
7 1 0
9 9 9
1 23 12
6 5 9
1         <- sort above records by index specified here

Sample Output
7 1 0
10 2 5
6 5 9
9 9 9
1 23 12

'''

count, __ = raw_input().split()
l = list()
for i in xrange(int(count)):
    l.append(raw_input().split())

index = int(raw_input())

l_sorted = sorted(l, key=lambda x: int(x[index]))
for i in l_sorted:
    print ' '.join(i)
